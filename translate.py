#!/usr/bin/env python3

import convert
import sys

NOTRANS = 0
TRANS = 1

def pc(c):
    print(c, end="")

def do(type : str, basefile : str, writefile : str, mark : str = "Generated", verbose : bool = False):
    if verbose: print("Beginning translation for " + type)
    if type in convert.types or type == "blank":
        pass
    elif type.startswith("argos_"):
        try:
            from argostranslate import package, translate
        except ImportError:
            print("You need the package 'argostranslate' to perform this translation")
            quit()
    else:
        print("Requested language not available: " + type)
        quit()

    en = None
    transTo = None
    langs = None
    argos_translator = None

    if type.startswith("argos_"):
        langs = translate.get_installed_languages()
        for lang in langs:
            if str(lang) == "English":
                en = lang
            elif str(lang) == type[6:]:
                transTo = lang
        if en != None and transTo != None:
            argos_translator = en.get_translation(transTo)
        else:
            print("Couldn't find the specified languages")
            quit()

    attributes = {}
    comments = []
    if mark != "":
        comments.append("# " + mark + "\n")
    with open(basefile, "r", encoding="utf-8") as f:
        key = ""
        text = []
        noKeyInNextLine = False
        for l in f:
            if l.startswith("#"):
                comments.append(l)
            else:
                skip = 0
                if noKeyInNextLine:
                    isKey = False
                    noKeyInNextLine = False
                else:
                    key = ""
                    isKey = True
                    text = []
                checkNext = False
                direct = 0
                for i, c in enumerate(l):
                    if checkNext and not isKey:
                        if c == "\\":
                            text.append([TRANS, "\\"])
                        elif c == "%":
                            text.append([TRANS, "\\%"])
                        elif c == "n":
                            text.append([NOTRANS, "\\n"])
                        elif c == "u":
                            text.append([NOTRANS, "\\u"])
                            direct = 4
                        elif c == "\n":
                            text.append([NOTRANS, "\\\n"])
                            noKeyInNextLine = True
                        checkNext = False
                    elif c == "%":
                        text.append([NOTRANS, "%"])
                        direct = 1
                    elif c == "_" and argos_translator:
                        pass
                    elif direct > 0 and not isKey:
                        text[-1][1] += c
                        direct -= 1
                    elif c == "=":
                        isKey = False
                    elif c == "\\":
                        checkNext = True
                    elif c == "\n":
                        pass
                    else:
                        if isKey:
                            key += c
                        else:
                            text.append([TRANS, c])
                if noKeyInNextLine:
                    pass
                else:
                    attributes[key] = text

    with open(writefile, "w", encoding="utf-8") as f:
        for l in comments:
            f.write(l)
        for key, text in attributes.items():
            writeText = key + "="
            transText = ""
            if type == "upside-down":
                text.reverse()
            elif type == "blank":
                text = []
            for c in text:
                if argos_translator != None:
                    transText += c[1]
                else:
                    if c[0] == NOTRANS:
                        writeText += c[1]
                    elif c[0] == TRANS:
                        writeText += convert.convert(type, c[1])
            if argos_translator != None and not key.startswith("game.token."):
                translatedText = argos_translator.translate(transText)
                if verbose: print(transText + " : " + translatedText)
                translatedText = translatedText.replace("\\\n", "gfunoigsuiogrwungwiioqwdnmiopcds")
                translatedText = translatedText.replace("\n", "\\\n")
                translatedText = translatedText.replace("gfunoigsuiogrwungwiioqwdnmiopcds", "\\\n")
                translatedText = translatedText.replace("%s", "gfunoigsuiogrwungwiioqwdnmiopcds")
                translatedText = translatedText.replace("%", "%s")
                translatedText = translatedText.replace("gfunoigsuiogrwungwiioqwdnmiopcds", "%s")

                writeText += translatedText
            f.write(writeText + "\n")
