#!/usr/bin/env python3.9
import sort
import translate


VERBOSE = False

def path(filename : str):
    return "/home/q/IdeaProjects/swp2020a/client/src/main/resources/i18n/" + filename + ".properties"

base = path("SWP2020A")

sort.do(path("SWP2020A"))
sort.do(path("SWP2020A_de_DE"))
sort.do(path("SWP2020A_de_DE_sie"))
sort.do(path("SWP2020A_en_GB_improved"))
sort.do(path("SWP2020A_en_GB_UwU"))
sort.do(path("SWP2020A_nds_DE"))

translate.do("pseudo-cyrillic", base, path("SWP2020A_en_GB_pseudo-cyrillic"), verbose = VERBOSE)
translate.do("playing-cards", base, path("SWP2020A_en_GB_playing-cards"), verbose = VERBOSE)
translate.do("upside-down", base, path("SWP2020A_en_GB_upside-down"), verbose = VERBOSE)
translate.do("braille", base, path("SWP2020A_en_GB_blind"), verbose = VERBOSE)
translate.do("upper", base, path("SWP2020A_en_GB_hearing-impaired"), verbose = VERBOSE)
translate.do("blank", base, path("SWP2020A_en_GB_blank"), verbose = VERBOSE)
translate.do("lower", base, path("SWP2020A_en_GB_lowercase"), verbose = VERBOSE)
translate.do("upper", path("SWP2020A_nds_DE"), path("SWP2020A_nds_DE_for-olle-luu"), verbose = VERBOSE)
translate.do("upper", path("SWP2020A_en_GB_UwU"), path("SWP2020A_en_GB_uwu-scweam"), verbose = VERBOSE)
translate.do("lower", path("SWP2020A_en_GB_UwU"), path("SWP2020A_en_GB_uwu-wowewcase"), verbose = VERBOSE)
translate.do("argos_German", base, path("SWP2020A_de_DE_generated"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Arabic", base, path("SWP2020A_ar_SA"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Chinese", base, path("SWP2020A_zh_CN"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_French", base, path("SWP2020A_fr_FR"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Hindi", base, path("SWP2020A_hi_IN"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Indonesian", base, path("SWP2020A_id_ID"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Irish", base, path("SWP2020A_ga_IE"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Italian", base, path("SWP2020A_it_IT"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Japanese", base, path("SWP2020A_ja_JP"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Korean", base, path("SWP2020A_ko_KR"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Polish", base, path("SWP2020A_pl_PL"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Portuguese", base, path("SWP2020A_pt_PR"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Russian", base, path("SWP2020A_ru_RU"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Spanish", base, path("SWP2020A_es_ES"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Turkish", base, path("SWP2020A_tr_TR"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)
translate.do("argos_Vietnamese", base, path("SWP2020A_vi_VN"), mark = "Generated (Dubious Quality)", verbose = VERBOSE)

